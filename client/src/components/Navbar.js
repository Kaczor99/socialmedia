import React, { useEffect, useState } from "react";
import "./Navbar.css";
function Navbar() {
  const [loggedIn, setLoggedIn] = useState(true);
  useEffect(() => {
    //console.log(localStorage.getItem("loggedIn"));
    setLoggedIn(localStorage.getItem("loggedIn"));
    //console.log(loggedIn);
  }, [localStorage.getItem("loggedIn")]);

    const changeLang = () => {
        if (localStorage.getItem("jezyk") === "false") {
            document.getElementById("home").innerHTML = "Strona glowna";
            document.getElementById("upload").innerHTML = "Dodaj post";
            document.getElementById("profile").innerHTML = "Profil";
            document.getElementById("jezyk").innerHTML = "Zmien jezyk";
            localStorage.setItem("jezyk", true);
        }
        else {
            document.getElementById("home").innerHTML = "Home";
            document.getElementById("profile").innerHTML = "Profile";
            document.getElementById("jezyk").innerHTML = "Change language";
            document.getElementById("upload").innerHTML = "Upload";
            localStorage.setItem("jezyk", false);

        }
    };

  return (
      <div className="Navbar">
          <button id="jezyk" onClick={changeLang} >Change language</button>
          <a id="home" href="/">Home</a>
          
          {loggedIn ? (
        <>
          <a id="upload" href="/upload">Upload</a>
          <a id="profile" href="/profile">Profile</a>
          <a id="logout" href="/login">Logout</a>
        </>
      ) : (
        <>
          <a id="register" href="/register">Register</a>
          <a id="login" href="/login">Login</a>
        </>
      )}
    </div>
  );
}

export default Navbar;
